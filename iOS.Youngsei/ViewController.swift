//
//  ViewController.swift
//  iOS.Youngsei
//
//  Created by TaeJin Kim on 09/07/2019.
//  Copyright © 2019 TaeJin Kim. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, UIScrollViewDelegate
{
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var webView: WKWebView!
    
    var activityIndicator = UIActivityIndicatorView()
    
    
    override func loadView()
    {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        
        webView.uiDelegate = self
        view = webView
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        webView.scrollView.delegate = self
        
        //URL을 통해서 웹뷰를 설정함
        let mainUrl = URL(string:"https://www.youngsei.net")
        let request = URLRequest(url: mainUrl!)
        
        webView.load(request)
        
    }
    
    // 리소스 관리 -> 앱 사용중에 메모리가 부족할 때 불리는 메소드임
    // 오랫동안 사용하지 않은 객체와 다시 쉽게 만들 수 있는 객체는 제거한다.
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //확대 및 축소방지하는 기능
    //UIScrollViewDelegate으로부터 상속받음
    //viewDidLoad() 메소드에서 scrollView.delegate를 통해서 구현
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        scrollView.setZoomScale(1.0, animated: false)
    }

}
